import Vue from 'vue'
import App from './App'
var  request = require("request.js")

Vue.config.productionTip = false
Vue.prototype.request = request;
Vue.prototype.appInfo = {
	inited:false,
	data:{},
	setData:function(data){
		this.data = data;
		this.inited = true;
		this.doChange();
	},
	get:function(name){
		return typeof this.data[name] != 'undefined' ? this.data[name] : null;
	},
	onChange:function(){
		
	},
	doChange:function(){
		this.onChange();
	},
	getUser:function(){
		return this.data.userinfo ? this.data.userinfo : {};
	},
	setUserInfo:function(data){
		this.data.userinfo = data ? data : {};
		return data ? data : {};
	},
	isLogin:function(){
		return this.getUser().username ? true : false;
	}
};

App.mpType = 'app'

const app = new Vue({
    ...App
})
app.$mount()
